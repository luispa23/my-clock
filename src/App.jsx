
import './App.scss';
import { StopWatch } from './components';
import Countdown from './components/CountDown/CountDown';
import DigitalClock from './components/DigitalClock/DigitalClock';

function App() {
  return (
    <div className="App">
      <DigitalClock/>
      <Countdown/>
      <StopWatch/>
    </div>
  );
}

export default App;
